L.TileLayer.ChinaProvider = L.TileLayer.extend({

  initialize: function(type, options) { // (type, Object)
    var providers = L.TileLayer.ChinaProvider.providers;

    var parts = type.split('.');

    var providerName = parts[0];
    var mapName = parts[1];
    var mapType = parts[2];

    var url = providers[providerName][mapName][mapType];
    options.subdomains = providers[providerName].Subdomains;

    L.TileLayer.prototype.initialize.call(this, url, options);
  }
});

L.TileLayer.ChinaProvider.providers = {
  TianDiTu: {
    Normal: {
      Map: "http://t{s}.tianditu.cn/DataServer?T=vec_w&X={x}&Y={y}&L={z}",
      Annotion: "http://t{s}.tianditu.cn/DataServer?T=cva_w&X={x}&Y={y}&L={z}",
    },
    Satellite: {
      Map: "http://t{s}.tianditu.cn/DataServer?T=img_w&X={x}&Y={y}&L={z}",
      Annotion: "http://t{s}.tianditu.cn/DataServer?T=cia_w&X={x}&Y={y}&L={z}",
    },
    Terrain: {
      Map: "http://t{s}.tianditu.cn/DataServer?T=ter_w&X={x}&Y={y}&L={z}",
      Annotion: "http://t{s}.tianditu.cn/DataServer?T=cta_w&X={x}&Y={y}&L={z}",
    },
    Subdomains: ['0', '1', '2', '3', '4', '5', '6', '7']
  },

  GaoDe: {
    Normal: {
      Map: 'http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
    },
    Satellite: {
      Map: 'http://webst0{s}.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}',
      Annotion: 'http://webst0{s}.is.autonavi.com/appmaptile?style=8&x={x}&y={y}&z={z}'
    },
    Subdomains: ["1", "2", "3", "4"]
  },

  Google: {
    Normal: {
      Map: "http://www.google.cn/maps/vt?lyrs=m@189&gl=cn&x={x}&y={y}&z={z}"
    },
    Satellite: {
      Map: "http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}"
    },
    Subdomains: []
  },

  Geoq: {
    Normal: {
      Map: "http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineCommunity/MapServer/tile/{z}/{y}/{x}",
      Color: "http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineStreetColor/MapServer/tile/{z}/{y}/{x}",
      PurplishBlue: "http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineStreetPurplishBlue/MapServer/tile/{z}/{y}/{x}",
      Gray: "http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineStreetGray/MapServer/tile/{z}/{y}/{x}",
      Warm: "http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineStreetWarm/MapServer/tile/{z}/{y}/{x}",
      Cold: "http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineStreetCold/MapServer/tile/{z}/{y}/{x}"
    },
    Subdomains: []

  }
};

L.tileLayer.chinaProvider = function(type, options) {
  return new L.TileLayer.ChinaProvider(type, options);
};

/**  
 * 智图地图内容  
 */
var normalm1 = L.tileLayer.chinaProvider('Geoq.Normal.Map', {
  maxZoom: 18,
  minZoom: 2
});
var normalm2 = L.tileLayer.chinaProvider('Geoq.Normal.Color', {
  maxZoom: 18,
  minZoom: 2
});
var normalm3 = L.tileLayer.chinaProvider('Geoq.Normal.PurplishBlue', {
  maxZoom: 18,
  minZoom: 2
});
var normalm4 = L.tileLayer.chinaProvider('Geoq.Normal.Gray', {
  maxZoom: 18,
  minZoom: 2
});
var normalm5 = L.tileLayer.chinaProvider('Geoq.Normal.Warm', {
  maxZoom: 18,
  minZoom: 2
});
var normalm6 = L.tileLayer.chinaProvider('Geoq.Normal.Cold', {
  maxZoom: 18,
  minZoom: 2
});
/**  
 * 天地图内容  
 */
var normalm = L.tileLayer.chinaProvider('TianDiTu.Normal.Map', {
    maxZoom: 18,
    minZoom: 2
  }),
  normala = L.tileLayer.chinaProvider('TianDiTu.Normal.Annotion', {
    maxZoom: 18,
    minZoom: 2
  }),
  imgm = L.tileLayer.chinaProvider('TianDiTu.Satellite.Map', {
    maxZoom: 18,
    minZoom: 2
  }),
  imga = L.tileLayer.chinaProvider('TianDiTu.Satellite.Annotion', {
    maxZoom: 18,
    minZoom: 2
  });

var normal = L.layerGroup([normalm, normala]),
  image = L.layerGroup([imgm, imga]);
/**  
 * 谷歌  
 */
var normalMap = L.tileLayer.chinaProvider('Google.Normal.Map', {
    maxZoom: 18,
    minZoom: 2
  }),
  satelliteMap = L.tileLayer.chinaProvider('Google.Satellite.Map', {
    maxZoom: 18,
    minZoom: 2
  });
/**  
 * 高德地图  
 */
var Gaode = L.tileLayer.chinaProvider('GaoDe.Normal.Map', {
  maxZoom: 18,
  minZoom: 2
});
var Gaodimgem = L.tileLayer.chinaProvider('GaoDe.Satellite.Map', {
  maxZoom: 18,
  minZoom: 2
});
var Gaodimga = L.tileLayer.chinaProvider('GaoDe.Satellite.Annotion', {
  maxZoom: 18,
  minZoom: 2
});
var Gaodimage = L.layerGroup([Gaodimgem, Gaodimga]);

var baseLayers = {
  "智图地图": normalm1,
  //"智图多彩": normalm2,  
  "智图午夜蓝": normalm3,
  "智图灰色": normalm4,
  //"智图暖色": normalm5,  
  //"智图冷色": normalm6,  
  // "天地图": normal,  
  // "天地图影像": image,  
  // "谷歌地图": normalMap,  
  // "谷歌影像": satelliteMap,  
  // "高德地图": Gaode,  
  "高德影像": Gaodimage,

}