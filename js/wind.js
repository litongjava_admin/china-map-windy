function initDemoMap() {

  var map = L.map("map", {
    center: [35, 110],
    zoom: 8,
    layers: Gaodimage,
    zoomControl: false,
    attributionControl: false
  });

  L.control.zoom({
    zoomInTitle: '放大',
    zoomOutTitle: '缩小'
  }).addTo(map);

  return {
    map: map,
    //layerControl: layerControl
  };
}

// demo map
var mapStuff = initDemoMap();
var map = mapStuff.map;

$.getJSON('2017121300.json', function(data) {
  var velocityLayer = L.velocityLayer({
    displayValues: true,
    displayOptions: {
      velocityType: '',
      displayPosition: 'bottomleft',
      displayEmptyString: 'No wind data'
    },
    data: data,
    maxVelocity: 15
  });
  velocityLayer.addTo(map);

});