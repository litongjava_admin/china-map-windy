/**
 * 问题提出：浏览器向后台发送请求后，后台返回一个html界面。但是在浏览器中没有加载js、css等静态资源，查找愿意后发现是Content-Type的原因。浏览器不知道css、js等文件的文件格式，无法成功加载静态文件。所以，需要设置正确的文件格式。
 */

var http = require("http")
var fs = require("fs")

var port=3000;
http.createServer(function(req, res) {
  var path = req.url;
  console.log("path:" + path)
  if(path == "/") {
    path = "/index.html";
  }
  sendFile(res, path);
}).listen(port);
console.log("http://127.0.0.1:"+port);
function sendFile(res, path) {
  var path = process.cwd() + path;
  fs.readFile(path, function(err, stdout, stderr) {
    if(!err) {
      var data = stdout;
      var type = path.substr(path.lastIndexOf(".") + 1, path.length)
      res.writeHead(200, { 'Content-type': "text/" + type }); //在这里设置文件类型，告诉浏览器解析方式
      res.write(data);
    }
    res.end();
  })
}